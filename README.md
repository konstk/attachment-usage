#### Plugin Name: Attachment Usage
Find easily the usage of attachments in different locations on the website.

#### Description 

***Ever lost the overview of your attachments in the media library? This plugin helps you
to identify if an attachment has been used on the website. A free and easy to use plugin 
saving you time working in Wordpress.***

#### Work more efficient

By installing this plugin you can focus on your website and do not need to keep track of 
where you used a specific attachment. 

#### Locations looked through

* Posts, pages: It searches in content and excerpt field as well as checks the attached thumbnail.
* Widgets: It searches in the following widget elements (text, audio, video, gallery, image).
* WooCommerce Products: It searches in content and excerpt field as well as checks the product thumbnail and product gallery.
* WooCommerce Product Variation: It looks through the product variation thumbnail if the image is used.
* WooCommerce Product Category: It searches if the image is set in a product category.

#### Compatible

The plugin is compatible with a multisite setup and WooCommerce. Furthermore it supports
the lookup process for the following page builders Gutenberg, Elementor and Visual Composer.

#### Installation

Quick and easy installation:

1. Integrate source into local development environment or create a zip file `attachment-usage` and upload to your plugin directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. That´s it!

#### Information

Further information will follow.

#### License
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html