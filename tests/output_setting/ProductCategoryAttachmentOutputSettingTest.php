<?php

class ProductCategoryAttachmentOutputSettingTest extends WP_UnitTestCase{
    
    private $cases = array(
        'wc-category' => array(
            'section-title' => 'Product Category',
            'location-info' => '(in product category)'
        )
    );
    private $post_id;
    private $second_post_id;
    
    public function setUp(){
        parent::setUp();
              
        $new_post = array(
            'post_title' => 'Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $this->post_id = wp_insert_post($new_post);
        
        register_taxonomy(
            'product_cat',
            'products',
            array(
                'label' => __( 'Product Category' ),
                'rewrite' => array( 'slug' => 'product_cat' ),
                'hierarchical' => true,
            )
        );
        
        $term = wp_insert_term(
            'Belts', // the term 
            'product_cat', // the taxonomy
            array(
                'description'=> "nice belts",
                'slug' => "belts"
            )
        );
        $this->cat_id = $term['term_id'];
        
        include_once dirname(__FILE__).'/../Attachment.php';
        $attachment = new Attachment('dummy_data/bild1-compressed.jpg');
        $attachment_id = $attachment->get_attachment_id();
        update_term_meta($this->cat_id, 'thumbnail_id', absint( $attachment_id ) );
        
    }
    
    public function test_output_infos(){
        foreach($this->cases as $key => $val){
            $location_info = $val['location-info'];
            $section_title = $val['section-title'];
            $attachment_output_obj = new AttachmentUsage\Core\OutputSetting\Product_Category_Attachment_Output_Setting($location_info, $section_title);
            $this->assertEquals($attachment_output_obj->get_location_info(), $location_info);
            $this->assertEquals($attachment_output_obj->get_section_title(), $section_title);
            $this->assertEquals($attachment_output_obj->get_title($this->cat_id), 'Belts');
            $this->assertEquals($attachment_output_obj->get_edit_link($this->cat_id), get_edit_term_link($this->cat_id));        
        }
    }
    
}
