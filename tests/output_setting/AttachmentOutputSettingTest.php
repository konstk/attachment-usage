<?php

class AttachmentOutputSettingTest extends WP_UnitTestCase{
    
    private $cases = array(
        'wc-gallery' => array(
            'section-title' => 'Product Attachment',
            'location-info' => '(in product gallery)'
        ),
        'product' => array(
            'section-title' => 'Product Content',
            'location-info' => '(in product content)'
        ),
        'post' => array(
            'section-title' => 'Post Content',
            'location-info' => '(in post content)'
        ),
        'page' => array(
            'section-title' => 'Page Content',
            'location-info' => '(in page content)'
        ),
        'thumbnail' => array(
            'section-title' => 'Post Attachment',
            'location-info' => '(thumbnail)'
        )
    );
    private $post_id;
    private $second_post_id;
    
    public function setUp(){
        parent::setUp();
              
        $new_post = array(
            'post_title' => 'Table Tennis',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'post'
        );
        $this->post_id = wp_insert_post($new_post);
        
            $new_post = array(
            'post_title' => '',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'post'
        );
        $this->second_post_id = wp_insert_post($new_post);
    }
    
    
    public function test_output_infos(){
        foreach($this->cases as $key => $val){
            $location_info = $val['location-info'];
            $section_title = $val['section-title'];
            $attachment_output_obj = new AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting($location_info, $section_title);
            $this->assertEquals($attachment_output_obj->get_location_info(), $location_info);
            $this->assertEquals($attachment_output_obj->get_section_title(), $section_title);
        }
    }
    
    public function test_get_edit_link(){
        $location_info = $this->cases['post']['location-info'];
        $section_title = $this->cases['post']['section-title'];
        $attachment_output_obj = new AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting($location_info, $section_title);
        $edit_link = get_edit_post_link($this->post_id);
        $this->assertEquals($attachment_output_obj->get_edit_link($this->post_id), $edit_link);
    }
    
    public function test_empty_title(){
        $location_info = $this->cases['post']['location-info'];
        $section_title = $this->cases['post']['section-title'];
        $attachment_output_obj = new AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting($location_info, $section_title);
        $edit_link = get_edit_post_link($this->second_post_id);
        $this->assertEquals($attachment_output_obj->get_title($this->second_post_id), $edit_link);
    }
    
    public function test_not_empty_title(){
        $location_info = $this->cases['post']['location-info'];
        $section_title = $this->cases['post']['section-title'];
        $attachment_output_obj = new AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting($location_info, $section_title);
        $this->assertEquals($attachment_output_obj->get_title($this->post_id), 'Table Tennis');
    }
}

