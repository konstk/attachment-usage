<?php

class AttachmentOutputSettingFactoryTest extends WP_UnitTestCase{
    
    public function test_widget_object(){
        $attachment_output_factory = new \AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting_Factory('sidebar');
        $attachment_output_obj = $attachment_output_factory->get_object();
        $class = \AttachmentUsage\Core\OutputSetting\Widget_Attachment_Output_Setting::class;
        $this->assertInstanceOf($class, $attachment_output_obj);
    }
    
    public function test_product_categoy_object(){
        $attachment_output_factory = new \AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting_Factory('wc-category');
        $attachment_output_obj = $attachment_output_factory->get_object();
        $class = \AttachmentUsage\Core\OutputSetting\Product_Category_Attachment_Output_Setting::class;
        $this->assertInstanceOf($class, $attachment_output_obj); 
    }
    
    public function test_default_object(){
        $types = ['wc-gallery','product','post','page','thumbnail'];
         $class = \AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting::class;
        foreach($types as $type){
            $attachment_output_factory = new \AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting_Factory($type);
            $attachment_output_obj = $attachment_output_factory->get_object();
            $this->assertInstanceOf($class, $attachment_output_obj); 
        }
    }
    
}