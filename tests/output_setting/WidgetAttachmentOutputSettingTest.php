<?php

class WidgetAttachmentOutputSettingTest extends WP_UnitTestCase{
    
    private $case = array(
        'section-title' => 'Widgets',
        'location-info' => '(in widget)',
        'widgets' => array('text-23', 'image-15'),
        'sidebar' => 'sidebar-23'
    );

    
    public function test_output_infos(){
        $location_info = $this->case['location-info'];
        $section_title = $this->case['section-title'];
        $url = admin_url("widgets.php?show_widgets=sidebar-23&widget_elements=text-23,image-15");
        $attachment_output_obj = new AttachmentUsage\Core\OutputSetting\Widget_Attachment_Output_Setting($location_info, $section_title);
        $attachment_output_obj->set_results(array($this->case['sidebar'] => $this->case['widgets']));
        $this->assertEquals($attachment_output_obj->get_location_info(), $location_info);
        $this->assertEquals($attachment_output_obj->get_section_title(), $section_title);
        $this->assertEquals($attachment_output_obj->get_title($this->case['sidebar']), 'Widgets-Sidebar-23');
        $this->assertEquals($attachment_output_obj->get_edit_link($this->case['sidebar']), $url);
    }
}