<?php

class ResultBuilderControllerTest extends WP_UnitTestCase{

    private $usage_found = array(
        'found' => array(
            18 => array(
                'page' => array(2,15),
                'post' => array(4,10)
            ),
            122 => array(
                'page' => array(4,15),
                'post' => array(1,10),
                'product' => array(17)
            ),
        ),
        'not-found' => array(
            33, 55
        )
    );
    
    public function setUp(){
        parent::setUp();
        update_option('au_attachment_usage_found', $this->usage_found);
    }
    
    private function get_attachment_id($found_status){
        if($found_status === 'found'){
            $found_ids = array_keys($this->usage_found['found']);
            $attachment_id = $found_ids[0];
        }else if($found_status === 'not-found'){
            $not_found_ids = $this->usage_found['not-found'];
            $attachment_id = $not_found_ids[0];
        }
        return $attachment_id;
    }
    
    public function test_is_attachment_found(){
        $attachment_id = $this->get_attachment_id('found');
        $result_builder_controller = new \AttachmentUsage\Core\ResultBuilder\Result_Builder_Controller($attachment_id);
        $this->assertTrue($result_builder_controller->is_attachment_found());       
    }
    
    public function test_is_attachment_not_found(){
        $attachment_id = $this->get_attachment_id('not-found');
        $result_builder_controller = new \AttachmentUsage\Core\ResultBuilder\Result_Builder_Controller($attachment_id);
        $this->assertFalse($result_builder_controller->is_attachment_found());       
    }
    
    public function test_get_result_found_before_building_result(){
        $attachment_id = $this->get_attachment_id('found');
        $result_builder_controller = new \AttachmentUsage\Core\ResultBuilder\Result_Builder_Controller($attachment_id);
        $this->assertEquals($result_builder_controller->get_result(), 'Attachment not found');
    }
    
    public function test_get_result_found_attachment(){
        $attachment_id = $this->get_attachment_id('found');
        $result_builder_controller = new \AttachmentUsage\Core\ResultBuilder\Result_Builder_Controller($attachment_id);
        $result_builder_controller->build_result();
        $this->assertNotEquals($result_builder_controller->get_result(), 'Attachment not found');
    }
    
    public function test_get_result_not_found_attachment(){
        $attachment_id = $this->get_attachment_id('not-found');
        $result_builder_controller = new \AttachmentUsage\Core\ResultBuilder\Result_Builder_Controller($attachment_id);
        $this->assertEquals($result_builder_controller->get_result(), 'Attachment not found');
    }
    
}