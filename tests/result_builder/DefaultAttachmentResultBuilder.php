<?php

class DefaultAttachmentResultBuilderTest extends WP_UnitTestCase{
    
    private $usage_found = array(   
        'page' => array(2,15),
        'post' => array(4,10),
        'product' => array(17),
        'wc-gallery' => array(16),
        'thumbnail' => array(25)
    );
            
    private function get_attachment_output_setting($type){
        $attachment_output_factory = new \AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting_Factory($type);
        return $attachment_output_factory->get_object();
    }
    
    private function build_result($type, $output_setting){
         $str = '';
        if(count($this->usage_found[$type]) > 0){
            $str = '<h4 class>'.$output_setting->get_section_title().'</h4>';
            foreach($this->usage_found[$type] as $output_setting => $val){
                $str .= '<a href="'.$output_setting->get_edit_link($val).'">'
                        .$output_setting->get_title($val).'</a> '.$output_setting->get_location_info().'</br>';
            }
        }
        return $str;
    }
    
    public function test_default_result_builder(){
        foreach($this->usage_found as $type => $val){
            $default_result_builder = new AttachmentUsage\Core\ResultBuilder\Default_Attachment_Result_Builder(
                    $val, 
                    new \AttachmentUsage\Core\OutputSetting\Attachment_Output_Setting_Factory($type)
                    );
            $result = $default_result_builder->get_usage_output();
            $built_result = $this->build_result($type, $this->get_attachment_output_setting($type));
            $this->assertEquals($result, $built_result);
        }
    }
}