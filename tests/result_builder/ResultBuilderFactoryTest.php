<?php

class ResultBuilderFactoryTest extends WP_UnitTestCase{
    
    private $cases = array(
        'sidebar' => array(
            'data' => array(
                'page' => array(2,15)
            ),
            'object' => AttachmentUsage\Core\ResultBuilder\Widget_Attachment_Result_Builder::class
        ),
        'page' => array(
            'data' => array(
                'page' => array(2,15),
                'post' => array(4,10),
                'product' => array(8,11)
            ),
            'object' => AttachmentUsage\Core\ResultBuilder\Default_Attachment_Result_Builder::class
        )
    );
    
    
    public function test_get_object(){
        foreach($this->cases as $key => $val){
            $type = $key;
            $data = $val['data'];
            $result_builder_factory = new AttachmentUsage\Core\ResultBuilder\Result_Builder_Factory($type, $data);
            $this->assertInstanceOf($val['object'], $result_builder_factory->get_object());
        }
    }
    
}