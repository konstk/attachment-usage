<?php

class ScreenHelperTest extends WP_UnitTestCase{
    
    
    public function test_upload_screen(){
        set_current_screen('upload');
        $screen_helper = new AttachmentUsage\Core\Screen_Helper();
        $this->assertTrue($screen_helper->is_screen_like('upload'));
        $this->assertTrue(!$screen_helper->is_mode_like('list'));
        $this->assertTrue(!$screen_helper->is_mode_like('grid'));
    }
    
    public function test_upload_list(){
        set_current_screen('upload');
        $GLOBALS['mode'] = 'list';
        $screen_helper = new AttachmentUsage\Core\Screen_Helper();
        $this->assertTrue($screen_helper->is_screen_like('upload'));
        $this->assertTrue($screen_helper->is_mode_like('list'));
        $this->assertTrue(!$screen_helper->is_mode_like('grid'));
    }
    
    public function test_upload_grid(){
        set_current_screen('upload');
        $GLOBALS['mode'] = 'grid';
        $screen_helper = new AttachmentUsage\Core\Screen_Helper();
        $this->assertTrue($screen_helper->is_screen_like('upload'));
        $this->assertTrue(!$screen_helper->is_mode_like('list'));
        $this->assertTrue($screen_helper->is_mode_like('grid'));
    }
    
    public function test_is_screen_in_arr(){
        set_current_screen('page');
        $screen_helper = new AttachmentUsage\Core\Screen_Helper();
        $this->assertTrue($screen_helper->is_screen_like('page'));
        $this->assertTrue($screen_helper->is_screen_in_arr(['page', 'product', 'post']));
    }
}