<?php

class ThumbnailContentHolderTest extends WP_UnitTestCase{

    private $posts = array();
    
    
    public function setUp(){
        parent::setUp();
    }
            
    private function add_post_with_thumbnail(){
        $new_post = array(
            'post_title' => 'Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $post_id = wp_insert_post($new_post);
        
        update_post_meta($post_id, '_thumbnail_id', '15');
    }
    
    public function test_fetch_content(){
        global $wpdb;
        $this->add_post_with_thumbnail();
        $thumbnail_content_holder = new AttachmentUsage\Core\ContentHolder\Thumbnail_Content_Holder($wpdb);
        $this->assertEquals(count($thumbnail_content_holder->get_content()), 1);
        $this->add_post_with_thumbnail();
        $thumbnail_content_holder = new AttachmentUsage\Core\ContentHolder\Thumbnail_Content_Holder($wpdb);
        $this->assertEquals(count($thumbnail_content_holder->get_content()), 2);
    }
    
}