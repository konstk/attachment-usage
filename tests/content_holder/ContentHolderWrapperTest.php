<?php

class ContentHolderWrapperTest extends WP_UnitTestCase{

    private $cases = array(
        'thumbnail' => AttachmentUsage\Core\ContentHolder\Thumbnail_Content_Holder::class,
        'wc-gallery' => AttachmentUsage\Core\ContentHolder\Gallery_Content_Holder::class,
        'wc-category' => AttachmentUsage\Core\ContentHolder\Product_Category_Content_Holder::class,
        'product' => AttachmentUsage\Core\ContentHolder\Content_Holder::class,
        'post' => AttachmentUsage\Core\ContentHolder\Content_Holder::class,
        'page' => AttachmentUsage\Core\ContentHolder\Content_Holder::class,
        'widget' => AttachmentUsage\Core\ContentHolder\Widget_Content_Holder::class,
    );
    
    private $posts = array();
    private $content_holder_wrapper;
    
    
    public function setUp(){
        parent::setUp();
        
        $new_post = array(
            'post_title' => 'Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $this->posts['product'] = wp_insert_post($new_post);
        
        $new_post = array(
            'post_title' => 'Table Tennis Page',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'page'
        );
        $this->posts['page'] = wp_insert_post($new_post);
        
        $new_post = array(
            'post_title' => 'Table Tennis Post',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'post'
        );
        $this->posts['post'] = wp_insert_post($new_post);
        $this->content_holder_wrapper = new AttachmentUsage\Core\ContentHolder\Content_Holder_Wrapper();
    }
            
    public function test_get_holder(){
        foreach($this->cases as $type => $val){
            $this->assertInstanceOf($val, $this->content_holder_wrapper->get_holder($type));
        }
    }
    
    public function test_page_content_holder(){
        $type = 'page';
        $content = $this->content_holder_wrapper->get_holder($type);
        $this->assertNotEmpty($content->get_content());
    }
    
    public function test_post_content_holder(){
        $type = 'post';
        $content = $this->content_holder_wrapper->get_holder($type);
        $this->assertNotEmpty($content->get_content());
    }
    
    public function test_product_content_holder(){
        $type = 'product';
        $content = $this->content_holder_wrapper->get_holder($type);
        $this->assertNotEmpty($content->get_content());
    }
    
}