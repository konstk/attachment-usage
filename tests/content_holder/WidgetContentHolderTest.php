<?php

class WidgetContentHolderTest extends WP_UnitTestCase{
    
    
    public function setUp(){
        parent::setUp();
    }

    public function test_fetch_content_image_type(){
        global $wpdb;
        $widget_content_holder = new \AttachmentUsage\Core\ContentHolder\Widget_Content_Holder($wpdb);
        $content = $widget_content_holder->get_content('image');
        $this->assertTrue(!array_key_exists('widget_media_audio', $content['widgets']));
        $this->assertTrue(!array_key_exists('widget_media_video', $content['widgets']));
    }
    
    public function test_fetch_content_no_image_type(){
        global $wpdb;
        $widget_content_holder = new \AttachmentUsage\Core\ContentHolder\Widget_Content_Holder($wpdb);
        $content = $widget_content_holder->get_content();
        $this->assertTrue(!array_key_exists('widget_media_image', $content['widgets']));
        $this->assertTrue(!array_key_exists('widget_media_gallery', $content['widgets']));
    }
    
}