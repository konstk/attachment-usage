<?php

class GalleryContentHolderTest extends WP_UnitTestCase{

    private $posts = array();
    
    
    public function setUp(){
        parent::setUp();
              
        $new_post = array(
            'post_title' => 'Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $this->posts[] = wp_insert_post($new_post);
        
        update_post_meta($this->posts[0], '_product_image_gallery', '15,20');
    
        $new_post = array(
            'post_title' => 'New Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $this->posts[] = wp_insert_post($new_post);
        
        update_post_meta($this->posts[1], '_product_image_gallery', '5,25');        
    }
            
    public function test_fetch_content(){
        global $wpdb;
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Gallery_Content_Holder($wpdb);
        $this->assertEquals(count($gallery_content_holder->get_content()), 2);
    }
    
}