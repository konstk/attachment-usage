<?php

class ProductCategoryContentHolderTest extends WP_UnitTestCase{

    private $post_id;
    
    
    public function setUp(){
        parent::setUp();
              
        $new_post = array(
            'post_title' => 'Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $this->post_id = wp_insert_post($new_post);
        
    }
    
    private function insert_woocommerce_cat($cat_name, $cat_slug){
        register_taxonomy(
            'product_cat',
            'products',
            array(
                'label' => __( 'Product Category' ),
                'rewrite' => array( 'slug' => 'product_cat' ),
                'hierarchical' => true,
            )
        );
        
        $term = wp_insert_term(
            $cat_name, // the term 
            'product_cat', // the taxonomy
            array(
                'description'=> "nice belts",
                'slug' => $cat_slug
            )
        );
        $cat_id = $term['term_id'];
        
        include_once dirname(__FILE__).'/../Attachment.php';
        $attachment = new Attachment('dummy_data/bild1-compressed.jpg');
        $attachment_id = $attachment->get_attachment_id();
        update_term_meta($cat_id, 'thumbnail_id', absint( $attachment_id ) );     
    }
            
    public function test_fetch_content(){
        global $wpdb;
        $this->insert_woocommerce_cat('Belt', 'belt');
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Product_Category_Content_Holder($wpdb);
        $this->assertEquals(count($gallery_content_holder->get_content()), 1);
        $this->insert_woocommerce_cat('House', 'house');
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Product_Category_Content_Holder($wpdb);
        $this->assertEquals(count($gallery_content_holder->get_content()), 2);
    }
    
}