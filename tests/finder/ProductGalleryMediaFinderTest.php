<?php

class ProductGalleryMediaFinderTest extends WP_UnitTestCase{
    
    private $attachment_id;
    
    public function setUp(){
        parent::setUp();
        include_once dirname(__FILE__).'/../Attachment.php';
        $attachment = new Attachment('dummy_data/bild1-compressed.jpg');
        $this->attachment_id = $attachment->get_attachment_id();
    }
    
    private function get_post(){
        $new_post = array(
            'post_title' => 'Table Tennis Product',
            'post_content' => 'Table tennis or ping-pong is a sport in which two or four players hit a lightweight ball back and forth using a table tennis racket. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a ball played toward them only one bounce on their side of the table and must return it so that it bounces on the opposite side. Points are scored when a player fails to return the ball within the rules.',
            'post_status' => 'publish',
            'post_date' => date('Y-m-d H:i:s'),
            'post_author' => 1,
            'post_type' => 'product'
        );
        $this->post_id = wp_insert_post($new_post);
        return $this->post_id;
    }

    public function test_single_image_in_gallery(){
        update_post_meta($this->get_post(), '_product_image_gallery', $this->attachment_id);    
        global $wpdb;
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Gallery_Content_Holder($wpdb);
        $product_gallery_finder = new AttachmentUsage\Core\Finder\Product_Gallery_Media_Finder($gallery_content_holder, 'gallery');
                
        $guid = get_the_guid($this->attachment_id);
        $attached_file = get_post_meta($this->attached_file, '_wp_attached_file');
        $file_url_handler = new AttachmentUsage\Core\File_Url_Handler($attached_file, $guid);
        $media_file = new AttachmentUsage\Core\Media_File($this->attachment_id, 'image/', $file_url_handler);
        $product_gallery_finder->set_media_file($media_file);
        $this->assertNotEmpty($product_gallery_finder->get_result());
    }
    
    public function test_first_image_in_gallery(){
        update_post_meta($this->get_post(), '_product_image_gallery', $this->attachment_id.',157');    
        global $wpdb;
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Gallery_Content_Holder($wpdb);
        $product_gallery_finder = new AttachmentUsage\Core\Finder\Product_Gallery_Media_Finder($gallery_content_holder, 'gallery');
        
        $guid = get_the_guid($this->attachment_id);
        $attached_file = get_post_meta($this->attached_file, '_wp_attached_file');
        $file_url_handler = new AttachmentUsage\Core\File_Url_Handler($attached_file, $guid);
        $media_file = new AttachmentUsage\Core\Media_File($this->attachment_id, 'image/', $file_url_handler);
        $product_gallery_finder->set_media_file($media_file);
        
        $this->assertNotEmpty($product_gallery_finder->get_result());
    }
    
    public function test_middle_image_in_gallery(){
        update_post_meta($this->get_post(), '_product_image_gallery', '75,'.$this->attachment_id.',365');    
        global $wpdb;
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Gallery_Content_Holder($wpdb);
        $product_gallery_finder = new AttachmentUsage\Core\Finder\Product_Gallery_Media_Finder($gallery_content_holder, 'gallery');
        
        $guid = get_the_guid($this->attachment_id);
        $attached_file = get_post_meta($this->attached_file, '_wp_attached_file');
        $file_url_handler = new AttachmentUsage\Core\File_Url_Handler($attached_file, $guid);
        $media_file = new AttachmentUsage\Core\Media_File($this->attachment_id, 'image/', $file_url_handler);
        $product_gallery_finder->set_media_file($media_file);
        
        $this->assertNotEmpty($product_gallery_finder->get_result());
    }
    
    public function test_last_image_in_gallery(){
        update_post_meta($this->get_post(), '_product_image_gallery', '174,257,'.$this->attachment_id);    
        global $wpdb;
        $gallery_content_holder = new AttachmentUsage\Core\ContentHolder\Gallery_Content_Holder($wpdb);
        $product_gallery_finder = new AttachmentUsage\Core\Finder\Product_Gallery_Media_Finder($gallery_content_holder, 'gallery');
        
        $guid = get_the_guid($this->attachment_id);
        $attached_file = get_post_meta($this->attached_file, '_wp_attached_file');
        $file_url_handler = new AttachmentUsage\Core\File_Url_Handler($attached_file, $guid);
        $media_file = new AttachmentUsage\Core\Media_File($this->attachment_id, 'image/', $file_url_handler);
        $product_gallery_finder->set_media_file($media_file);
        
        $this->assertNotEmpty($product_gallery_finder->get_result());
    }
        
}
