<?php

class Attachment{
    
    private $path;
    private $attach_id;
    
    
    public function __construct($path) {
        $this->path = $path;
        $this->upload_attachment();
    }
    
    private function upload_attachment(){
        $image_url = $this->path;
        $upload_dir = wp_upload_dir();
        $image_data = file_get_contents(__DIR__.'/'.$image_url );
        $filename = basename( $image_url );

        if ( wp_mkdir_p( $upload_dir['path'] ) ) {
          $file = $upload_dir['path'] . '/' . $filename;
        }
        else {
          $file = $upload_dir['basedir'] . '/' . $filename;
        }

        file_put_contents( $file, $image_data );
        $wp_filetype = wp_check_filetype( $filename, null );

        $attachment = array(
          'post_mime_type' => $wp_filetype['type'],
          'post_title' => sanitize_file_name( $filename ),
          'post_content' => '',
          'post_status' => 'inherit'
        );

        $this->attach_id = wp_insert_attachment( $attachment, $file );
        #require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $attach_data = wp_generate_attachment_metadata( $this->attach_id, $file );
        wp_update_attachment_metadata( $this->attach_id, $attach_data );
    }
    
    public function get_attachment_id(){
        return $this->attach_id;
    }
    
}