<?php

class ValidatorFactoryTest extends WP_UnitTestCase{
    
    private $setting;
    private $option_group;
    private $option_name;
    private $field;
    private $fields;
    
    public function setUp(){
        parent::setUp();
        $this->option_group = 'post_usage_page';
        $this->option_name = 'option';
        $page_elements = include('data/page_elements.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->page_elements_holder->configure();
        $this->fields = $this->page_elements_holder->get_fields();
        $this->field = current($this->fields);
    }

    public function test_get_string_validator(){
        
        
        $args1 = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'string',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
                       
        $validator_factory = new AttachmentUsage\SettingsLib\Elements\Validators\Validator_Factory(
                $args1['sanitize_callback_type'],
                $this->option_name, 
                $this->field, 
                $args1);
        $this->assertInstanceOf(AttachmentUsage\SettingsLib\Elements\Validators\String_Validator::class,
                $validator_factory->get_object());
    }
    
    public function test_get_file_validator(){
        $args = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'file',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
        $validator_factory = new AttachmentUsage\SettingsLib\Elements\Validators\Validator_Factory(
                $args['sanitize_callback_type'],
                $this->option_name, 
                $this->field, 
                $args);
        
        $this->assertInstanceOf(AttachmentUsage\SettingsLib\Elements\Validators\File_Validator::class,
                $validator_factory->get_object());

    }
    
    public function test_get_radio_whitelist_validator(){
        $args = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'radio-whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
        $validator_factory = new AttachmentUsage\SettingsLib\Elements\Validators\Validator_Factory(
                $args['sanitize_callback_type'],
                $this->option_name, 
                $this->field, 
                $args);
        
        $this->assertInstanceOf(AttachmentUsage\SettingsLib\Elements\Validators\Radio_Whitelist_Validator::class,
                $validator_factory->get_object());

    }
    
    public function test_get_checkbox_whitelist_validator(){
        $args = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'checkbox-whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
        $validator_factory = new AttachmentUsage\SettingsLib\Elements\Validators\Validator_Factory(
                $args['sanitize_callback_type'],
                $this->option_name, 
                $this->field, 
                $args);
        
        $this->assertInstanceOf(AttachmentUsage\SettingsLib\Elements\Validators\Checkbox_Whitelist_Validator::class,
                $validator_factory->get_object());

    }
    
    public function test_get_dropdown_whitelist_validator(){
        $args = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'dropdown-whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
        $validator_factory = new AttachmentUsage\SettingsLib\Elements\Validators\Validator_Factory(
                $args['sanitize_callback_type'],
                $this->option_name, 
                $this->fields['post_usage_dropdown'],
                $args);
        
        $this->assertInstanceOf(AttachmentUsage\SettingsLib\Elements\Validators\Dropdown_Whitelist_Validator::class,
                $validator_factory->get_object());

    }
    
    public function test_get_number_validator(){
        $args = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'number',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
        $validator_factory = new AttachmentUsage\SettingsLib\Elements\Validators\Validator_Factory(
                $args['sanitize_callback_type'],
                $this->option_name, 
                $this->field, 
                $args);
        
        $this->assertInstanceOf(AttachmentUsage\SettingsLib\Elements\Validators\Number_Validator::class,
                $validator_factory->get_object());
       
    }
    
}

