<?php

return array(
    'number' => array(
        'required' => array(
            array('value' => array(), 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => array('apfelbaum'), 'error' => 'The entered data is not valid - just numbers', 'output' => '0'),
            array('value' => '', 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'The entered data is not valid - just numbers', 'output' => '0'),
            array('value' => NULL, 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => TRUE, 'error' => 'The entered data is not valid - just numbers', 'output' => '0'),
            array('value' => 'tesntj   ><<>   - d', 'error' => 'The entered data is not valid - just numbers', 'output' => '0'),
            array('value' => FALSE, 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => 0, 'error' => NULL, 'output' => '0'),
            array('value' => '0', 'error' => NULL, 'output' => '0'),
            array('value' => 'first_cb', 'error' => 'The entered data is not valid - just numbers', 'output' => '0'),
            array('value' => '15.50', 'error' => NULL, 'output' => '15.50'),
            array('value' => '15.', 'error' => NULL, 'output' => '15.'),
            array('value' => '15.00', 'error' => NULL, 'output' => '15.00'),
            array('value' => '15', 'error' => NULL, 'output' => '15'),
            array('value' => 15, 'error' => NULL, 'output' => '15'),
            array('value' => 4.25, 'error' => NULL, 'output' => '4.25'),
            array('value' => 0.05, 'error' => NULL, 'output' => '0.05'),
            array('value' => '15,25', 'error' => 'The entered data is not valid - just numbers', 'output' => '0'),
        ),
        'not-required' => array(
            array('value' => array(), 'error' => 'The entered data is not valid - just numbers', 'output' => ''),
            array('value' => array('apfelbaum'), 'error' => 'The entered data is not valid - just numbers', 'output' => ''),
            array('value' => '', 'error' => NULL, 'output' => ''),
            array('value' => NULL, 'error' => 'The entered data is not valid - just numbers', 'output' => ''),
            array('value' => FALSE, 'error' => 'The entered data is not valid - just numbers', 'output' => ''),
            array('value' => TRUE, 'error' => 'The entered data is not valid - just numbers', 'output' => ''),
            array('value' => 0, 'error' => NULL, 'output' => '0'),
            array('value' => '0', 'error' => NULL, 'output' => '0'),
            array('value' => ' apfelbaum', 'error' => 'The entered data is not valid - just numbers', 'output' => ''),
            array('value' => '15.50', 'error' => NULL, 'output' => '15.50'),
            array('value' => '15.', 'error' => NULL, 'output' => '15.'),
            array('value' => '15.00', 'error' => NULL, 'output' => '15.00'),
            array('value' => '15', 'error' => NULL, 'output' => '15'),
            array('value' => 15, 'error' => NULL, 'output' => '15'),
            array('value' => 4.25, 'error' => NULL, 'output' => '4.25'),
            array('value' => 0.05, 'error' => NULL, 'output' => '0.05'),        
        ),       
    )
);

