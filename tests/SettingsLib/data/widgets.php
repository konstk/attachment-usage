<?php

return array(
    'sidebar-1' => array(
        array(
            'widgets' => array('audio', 'text', 'text'), 'file' => 'dummy_bilder/audio.mp3'
        ),
        array(
            'widgets' => array('text'), 'file' => 'dummy_bilder/audio.mp3'
        ),
        array(
            'widgets' => array('audio','text'), 'file' => 'dummy_bilder/audio.mp3'
        ),
        array(
            'widgets' => array('audio'), 'file' => 'dummy_bilder/audio.mp3'
        ),
        array(
            'widgets' => array('video','text', 'text'), 'file' => 'dummy_bilder/2017-03-29 12-16-02.flv'
        ),
        array(
            'widgets' => array('video','text'), 'file' => 'dummy_bilder/2017-03-29 12-16-02.flv'
        ),
        array(
            'widgets' => array('video'), 'file' => 'dummy_bilder/2017-03-29 12-16-02.flv'
        ),
        array(
            'widgets' => array('text'), 'file' => 'dummy_bilder/2017-03-29 12-16-02.flv'
        ),
        array(
            'widgets' => array('text', 'text'), 'file' => 'dummy_bilder/AGB.pdf'
        ),
        array(
            'widgets' => array('text'), 'file' => 'dummy_bilder/AGB.pdf'
        )
        
    ),
    'sidebar-2' => array(
        array(
            'widgets' => array('text'), 'file' => 'dummy_bilder/audio.mp3'
        ),
        array(
            'widgets' => array('video'), 'file' => 'dummy_bilder/2017-03-29 12-16-02.flv'
        )
    )
);

