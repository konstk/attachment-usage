<?php
return array(
    0 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_cb_setting',
        'args' => array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            )
        ),
    1 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_second_section'
    ),
    2 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_dropdown',
        'args' => array(
            'sanitize_callback_type' => 'whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            )
    ),
    3 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_second_input',
        'args' => array(
            'sanitize_callback_type' => 'string',
            'sanitize_callback_message' => __('The Input Field allows just text')
            )
    ),
    4 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_second_textarea'
    ),
    5 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_radio',
        'args' => array(
            'sanitize_callback_type' => 'whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            )
    ),
    6 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'post_usage_image'
    ),
    7 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'au_email_required'
    ),
    8 => array(
        'option_group' => 'post_usage_page',
        'option_name' => 'au_email'
    )
);
