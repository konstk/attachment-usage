<?php
return array(
    0 => array(
        'id' => 'post_usage_cb',
        'type' => 'checkbox',
        'title' => 'Checkbox 1',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage_second',
        'is_required' => FALSE,
        'option_name' => 'post_usage_second_section'
    ),
    1 => array(
        'id' => 'post_usage_cb2',
        'type' => 'checkbox',
        'title' => 'Checkbox 2',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage',
        'template' => 'other_cb',
        'group' => array(
            0 => array('id' => 'cb1', 'value' => 'first_cb'),
            1 => array('id' => 'cb2', 'value' => 'second_cb'),
            2 => array('id' => 'cb3', 'value' => 'third_cb')
            ),
        'option_name' => 'post_usage_cb_setting',
        'is_required' => TRUE,
        'group_option_name' => 'post_usage_cb_setting[]'
    ),
    2 => array(
        'id' => 'post_usage_dropdown',
        'type' => 'dropdown',
        'title' => 'Dropdown Field',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage',
        'option_name' => 'post_usage_second_dropdown',
        'dropdown_options' => array(
            "volvo" => "Volvo",
            "saab" => "Saab",
            "bmw" => "BMW"
        ),
        'is_required' => TRUE
    ),
    3 => array(
        'id' => 'post_usage_input',
        'type' => 'input',
        'title' => 'Input Field',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage_second',
        'is_required' => FALSE,
        'option_name' => 'post_usage_second_input',                               
    ),
    4 => array(
        'id' => 'post_usage_textarea',
        'type' => 'textarea',
        'title' => 'Textarea Field',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage_second',
        'is_required' => FALSE,
        'option_name' => 'post_usage_second_textarea',                               
    ),
    5 => array(
        'id' => 'post_usage_radio',
        'type' => 'radio',
        'title' => 'Radio Field',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage',
        'option_name' => 'post_usage_radio',
        'group' => array(
            0 => array('id' => 'radio1', 'value' => 'first_radio'),
            1 => array('id' => 'radio2', 'value' => 'second_radio'),
            2 => array('id' => 'radio3', 'value' => 'third_radio')
            ),
        'is_required' => TRUE
    ),
    6 => array(
        'id' => 'post_usage_image',
        'type' => 'image',
        'title' => 'Image Upload',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage',
        'option_name' => 'post_usage_image',
        'is_required' => FALSE,
    )
);