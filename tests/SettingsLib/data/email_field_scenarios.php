<?php

return array(
    'email' => array(
        'required' => array(
            array('value' => array(), 'error' => 'Field "%s" is required', 'output' => 'admin@admin.at'),
            array('value' => array('apfelbaum'), 'error' => 'No valid Email', 'output' => 'admin@admin.at'),
            array('value' => '', 'error' => 'Field "%s" is required', 'output' => 'admin@admin.at'),
            array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'No valid Email', 'output' => 'admin@admin.at'),
            array('value' => NULL, 'error' => 'Field "%s" is required', 'output' => 'admin@admin.at'),
            array('value' => TRUE, 'error' => 'No valid Email', 'output' => 'admin@admin.at'),
            array('value' => 'tesntj   ><<>   - d', 'error' => 'No valid Email', 'output' => 'admin@admin.at'),
            array('value' => FALSE, 'error' => 'Field "%s" is required', 'output' => 'admin@admin.at'),
            array('value' => 0, 'error' => 'Field "%s" is required', 'output' => 'admin@admin.at'),
            array('value' => 'admin@admin.at', 'error' => NULL, 'output' => 'admin@admin.at'),
            array('value' => 'adminadmin.at', 'error' => 'No valid Email', 'output' => 'admin@admin.at'),
            array('value' => 'test@wp.com', 'error' => NULL, 'output' => 'test@wp.com'),
        ),
        'not-required' => array(
            array('value' => array(), 'error' => 'No valid Email', 'output' => ''),
            array('value' => array('apfelbaum'), 'error' => 'No valid Email', 'output' => ''),
            array('value' => '', 'error' => NULL, 'output' => ''),
            array('value' => NULL, 'error' => 'No valid Email', 'output' => ''),
            array('value' => FALSE, 'error' => 'No valid Email', 'output' => ''),
            array('value' => TRUE, 'error' => 'No valid Email', 'output' => ''),
            array('value' => 0, 'error' => 'No valid Email', 'output' => ''),
            array('value' => '0', 'error' => 'No valid Email', 'output' => ''),
            array('value' => ' apfelbaum', 'error' => 'No valid Email', 'output' => ''),
            array('value' => 'admin@admin.at', 'error' => NULL, 'output' => 'admin@admin.at'),
            array('value' => 'adminadmin.at', 'error' => 'No valid Email', 'output' => ''),
            array('value' => 'test@wp.com', 'error' => NULL, 'output' => 'test@wp.com'),
        ),       
    )
);

