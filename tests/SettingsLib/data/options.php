<?php
return array(
    'page_title' => 'Post Usage',
    'menu_title' => 'Post Usage Settings',
    'capability' => 'manage_options',
    'menu_slug' => 'post_usage_page',
    'callback' => '',
    'keys' => array('page_title', 'menu_title', 'menu_slug'),
    'is_top_page' => True
    );
