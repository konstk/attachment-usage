<?php

return array(
    'checkbox' => array(
        'grouped' => array(
            'required' => array(
                array('value' => array(), 'error' => 'Field "%s" is required', 'output' => array('first_cb', 'third_cb')),
                array('value' => array('apfelbaum'), 'error' => 'Do not change value of field "%s"', 'output' => array('first_cb', 'third_cb')),
                array('value' => '', 'error' => 'Field "%s" is required', 'output' => array('first_cb', 'third_cb')),
                array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => array('first_cb', 'third_cb')),
                array('value' => NULL, 'error' => 'Field "%s" is required', 'output' => array('first_cb', 'third_cb')),
                array('value' => TRUE, 'error' => 'Do not change value of field "%s"', 'output' => array('first_cb', 'third_cb')),
                array('value' => FALSE, 'error' => 'Field "%s" is required', 'output' => array('first_cb', 'third_cb')),
                array('value' => 0, 'error' => 'Field "%s" is required', 'output' => array('first_cb', 'third_cb')),
                array('value' => '0', 'error' => 'Field "%s" is required', 'output' => array('first_cb', 'third_cb')),
                array('value' => array(0 => 'first_cb'), 'error' => NULL, 'output' => array(0 => 'first_cb')),
                array('value' => array(0 => 'third_cb'), 'error' => NULL,'output' => array(0 => 'third_cb')),
                array('value' => array(0 => 'third_cb', 1 => 'second_cb'),'error' => NULL, 'output' => array(0 => 'third_cb', 1 => 'second_cb')),
                array('value' => array(0 => 'third_cb', 1 => 'second_cb', 2 => 'first_cb'),'error' => NULL, 'output' => array(0 => 'third_cb', 1 => 'second_cb', 2 => 'first_cb')),
                array('value' => array(0 => 'Test', 1 => 'first_cb', 2 => 'first_cb', 3 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => array('first_cb', 'third_cb'))
            ),
            'not-required' => array(
                array('value' => array(),'error' => NULL, 'output' => array()),
                array('value' => array('apfelbaum'), 'error' => 'Do not change value of field "%s"', 'output' => array()),
                array('value' => '', 'error' => 'Do not change value of field "%s"','output' => array()),
                array('value' => NULL, 'error' => 'Do not change value of field "%s"','output' => array()),
                array('value' => FALSE,'error' => 'Do not change value of field "%s"', 'output' => array()),
                array('value' => TRUE, 'error' => 'Do not change value of field "%s"', 'output' => array()),
                array('value' => 0, 'error' => 'Do not change value of field "%s"','output' => array()),
                array('value' => '0', 'error' => 'Do not change value of field "%s"','output' => array()),
                array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => array()),
                array('value' => array(0 => 'Test', 1 => 'first_cb', 2 => 'first_cb', 3 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => array()),
                array('value' => array(0 => 'first_cb'),'error' => NULL, 'output' => array(0 => 'first_cb')),
                array('value' => array(0 => 'third_cb'),'error' => NULL, 'output' => array(0 => 'third_cb')),
                array('value' => array(0 => 'third_cb', 1 => 'second_cb'), 'error' => NULL,'output' => array(0 => 'third_cb', 1 => 'second_cb')),
                array('value' => array(0 => 'third_cb', 1 => 'second_cb', 2 => 'first_cb'), 'error' => NULL,'output' => array(0 => 'third_cb', 1 => 'second_cb', 2 => 'first_cb'))    
            )
        ),
        'single' => array(
            'not-required' => array(
                array('value' => '', 'error' => 'Do not change value of field "%s"','output' => NULL),
                array('value' => NULL, 'error' => NULL, 'output' => NULL),
                array('value' => FALSE, 'error' => 'Do not change value of field "%s"','output' => NULL),
                array('value' => TRUE,'error' => 'Do not change value of field "%s"', 'output' => NULL),
                array('value' => 1,'error' => 'Do not change value of field "%s"', 'output' => NULL),
                array('value' => 0, 'error' => 'Do not change value of field "%s"','output' => NULL),
                array('value' => '1', 'error' => NULL,'output' => TRUE),
                array('value' => '0', 'error' => 'Do not change value of field "%s"','output' => NULL),
                array('value' => array(),'error' => 'Do not change value of field "%s"', 'output' => NULL),
                array('value' => array('Dsfkjfd'),'error' => 'Do not change value of field "%s"', 'output' => NULL)
            )
        )
    ),
    'radio' => array(
        array('value' => array(),'error' => 'Field "%s" is required', 'output' => 'first_radio'),
        array('value' => 'apfelbaum', 'error' => 'Do not change value of field "%s"', 'output' => 'first_radio'),
        array('value' => '', 'error' => 'Field "%s" is required', 'output' => 'first_radio'),
        array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => 'first_radio'),
        array('value' => NULL, 'error' => 'Field "%s" is required', 'output' => 'first_radio'),
        array('value' => TRUE, 'error' => 'Do not change value of field "%s"', 'output' => 'first_radio'),
        array('value' => FALSE, 'error' => 'Field "%s" is required', 'output' => 'first_radio'),
        array('value' => 0, 'error' => 'Field "%s" is required', 'output' => 'first_radio'),
        array('value' => '0', 'error' => 'Field "%s" is required', 'output' => 'first_radio'),
        array('value' => array(0 => 'first_radio'),'error' => 'Do not change value of field "%s"', 'output' => 'first_radio'),
        array('value' => 'first_radio', 'error' => NULL,'output' => 'first_radio'),
        array('value' => 'third_radio', 'error' => NULL,'output' => 'third_radio'),
    ),
    'dropdown' => array(
        'not-required' => array(
            array('value' => array(), 'error' => 'Do not change value of field "%s"','output' => ''),
            array('value' => 'apfelbaum', 'error' => 'Do not change value of field "%s"', 'output' => ''),
            array('value' => '', 'error' => NULL,'output' => ''),
            array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => ''),
            array('value' => NULL,'error' => 'Do not change value of field "%s"', 'output' => ''),
            array('value' => TRUE, 'error' => 'Do not change value of field "%s"', 'output' => ''),
            array('value' => FALSE, 'error' => 'Do not change value of field "%s"','output' => ''),
            array('value' => 0, 'error' => 'Do not change value of field "%s"','output' => ''),
            array('value' => '0', 'error' => 'Do not change value of field "%s"','output' => ''),
            array('value' => array(0 => 'first_radio'),'error' => 'Do not change value of field "%s"', 'output' => ''),
            array('value' => 'volvo', 'error' => NULL,'output' => 'volvo'),
            array('value' => 'bmw', 'error' => NULL,'output' => 'bmw'),
        ),
        'required' => array(
            array('value' => array(), 'error' => 'Field "%s" is required', 'output' => 'bmw'),
            array('value' => 'apfelbaum', 'error' => 'Do not change value of field "%s"', 'output' => 'bmw'),
            array('value' => '', 'error' => 'Field "%s" is required', 'output' => 'bmw'),
            array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'Do not change value of field "%s"', 'output' => 'bmw'),
            array('value' => NULL, 'error' => 'Field "%s" is required', 'output' => 'bmw'),
            array('value' => TRUE, 'error' => 'Do not change value of field "%s"', 'output' => 'bmw'),
            array('value' => FALSE, 'error' => 'Field "%s" is required', 'output' => 'bmw'),
            array('value' => 0, 'error' => 'Field "%s" is required', 'output' => 'bmw'),
            array('value' => '0', 'error' => 'Field "%s" is required', 'output' => 'bmw'),
            array('value' => array(0 => 'first_radio'),'error' => 'Do not change value of field "%s"', 'output' => 'bmw'),
            array('value' => 'volvo','error' => NULL, 'output' => 'volvo'),
            array('value' => 'bmw','error' => NULL, 'output' => 'bmw'),
        ),
        'custom_default_value' => array(
            'required' => array(
                array('value' => array(), 'error' => 'Field "%s" is required', 'output' => 'custom_value'),                
            )
        )
    )
);

