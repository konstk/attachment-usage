<?php

return array(
    'file' => array(
        'required' => array(
            array('value' => array(), 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => array('apfelbaum'), 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '', 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => array(0 => 'Test', 1 => 'first_cb'), 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => NULL, 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => TRUE, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 'tesntj   ><<>   - d', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => FALSE, 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => 0, 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => '0', 'error' => 'Field "%s" is required', 'output' => '0'),
            array('value' => 'first_cb', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15.50', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15.', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15.00', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 15, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 5, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 4.25, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 0.05, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15,25', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
        ),
        'not-required' => array(
            array('value' => array(), 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => array('apfelbaum'), 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => NULL, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => FALSE, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => TRUE, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 0, 'error' => NULL, 'output' => '0'),
            array('value' => '0', 'error' => NULL, 'output' => '0'),
            array('value' => ' apfelbaum', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15.50', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15.', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15.00', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => '15', 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 15, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 4.25, 'error' => 'The entered data is no valid attachment id', 'output' => '0'),
            array('value' => 0.05, 'error' => 'The entered data is no valid attachment id', 'output' =>  '0'),        
        ),       
    )
);

