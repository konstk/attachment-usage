<?php

return array(
    'sidebars' => array(
        'sidebar-1' => array(
            'dummy_bilder/bild1.jpg' => array('image', 'image', 'text-image'),
            'dummy_bilder/bild2.jpg' => array('image'),
        ),
        'sidebar-2' => array(
            'dummy_bilder/bild1.jpg' => array('image', 'text-image'),
            'dummy_bilder/bild2.jpg' => array('text-image'),
        )
    ),
    'files' => array(
        'dummy_bilder/bild1.jpg' => 5,
        'dummy_bilder/bild2.jpg' => 2,
    )
);
