<?php

class WhitelistValidatorTest extends WP_UnitTestCase{
    
    private $setting;
    private $option_group;
    private $option_name;
    private $field;
    private $fields;
    private $is_required = TRUE;
    
    public function setUp(){
        parent::setUp();
        $this->option_group = 'post_usage_page';
        $this->option_name = 'option';
        $page_elements = include('data/page_elements.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->page_elements_holder->configure();
        $this->fields = $this->page_elements_holder->get_fields();
        $this->field = current($this->fields);                     
    }

    public function test_validate_group_checkbox_required(){
        $args = array(
                'id' => 'post_usage_cb2',
                'type' => 'checkbox',
                'title' => 'Checkbox 2',
                'calback' => '',
                'page' => 'post_usage_page',
                'section' => 'post_usage',
                'template' => 'other_cb',
                'group' => array(
                    0 => array('id' => 'cb1', 'title' => 'CB 1', 'value' => 'first_cb'),
                    1 => array('id' => 'cb2', 'title' => 'CB 2', 'value' => 'second_cb'),
                    2 => array('id' => 'cb3', 'title' => 'CB 3', 'value' => 'third_cb')
                    ),
                'option_name' => 'post_usage_cb_setting',
                'is_required' => TRUE,
                'group_option_name' => 'post_usage_cb_setting[]',
                'default_value' => array('first_cb', 'third_cb')
            );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Checkbox($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['checkbox']['grouped']['required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            $option_name = $key.'cb_required';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Checkbox_Whitelist_Validator($option_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }       
    }
    

    public function test_validate_group_checkbox_not_required(){
        $args = array(
                'id' => 'post_usage_cb2',
                'type' => 'checkbox',
                'title' => 'Checkbox 2',
                'calback' => '',
                'page' => 'post_usage_page',
                'section' => 'post_usage',
                'template' => 'other_cb',
                'group' => array(
                    0 => array('id' => 'cb1', 'value' => 'first_cb'),
                    1 => array('id' => 'cb2', 'value' => 'second_cb'),
                    2 => array('id' => 'cb3', 'value' => 'third_cb')
                    ),
                'option_name' => 'post_usage_cb_setting',
                'is_required' => FALSE,
                'group_option_name' => 'post_usage_cb_setting[]',
                'default_value' => array()
            );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Checkbox($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['checkbox']['grouped']['not-required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $option_name = $key.'group_not_required_cb';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Checkbox_Whitelist_Validator($option_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }
    }
        
    public function test_validate_single_checkbox_not_required(){
        $args = array(
                'id' => 'post_usage_cb2',
                'type' => 'checkbox',
                'title' => 'Checkbox 2',
                'calback' => '',
                'page' => 'post_usage_page',
                'section' => 'post_usage',
                'template' => 'other_cb',               
                'option_name' => 'post_usage_cb_setting_not_required_single',
                'is_required' => FALSE,
            );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Checkbox($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['checkbox']['single']['not-required'];
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $option_name = $key.'single_cb';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Checkbox_Whitelist_Validator($option_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }
    }
               
    public function test_validate_radio(){
        $args = array(
            'id' => 'post_usage_radio',
            'type' => 'radio',
            'title' => 'Radio Field',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'option_name' => 'post_usage_radio',
            'group' => array(
                0 => array('id' => 'radio1', 'value' => 'first_radio'),
                1 => array('id' => 'radio2', 'value' => 'second_radio'),
                2 => array('id' => 'radio3', 'value' => 'third_radio')
                ),
            'is_required' => TRUE,
            'default_value' => 'first_radio'
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Radio($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['radio'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            $option_name = $key.'radio';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Radio_Whitelist_Validator($option_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }

    }
    
    public function test_validate_dropdown_not_required(){
        $args = array(
            'id' => 'post_usage_dropdown',
            'type' => 'dropdown',
            'title' => 'Dropdown Field',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'option_name' => 'post_usage_second_dropdown',
            'dropdown_options' => array(
                "" => "Bitte auswählen",
                "volvo" => "Volvo",
                "saab" => "Saab",
                "bmw" => "BMW"
            ),
            'is_required' => FALSE,
            'default_value' => ''
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Dropdown($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['dropdown']['not-required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $option_name = $key.'drop_not';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Dropdown_Whitelist_Validator($option_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }
    }
    
    public function test_validate_dropdown_required(){
        $args = array(
            'id' => 'post_usage_dropdown',
            'type' => 'dropdown',
            'title' => 'Dropdown Field',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'option_name' => 'post_usage_second_dropdown',
            'dropdown_options' => array(
                "volvo" => "Volvo",
                "saab" => "Saab",
                "bmw" => "BMW"
            ),
            'is_required' => TRUE,
            'default_value' => 'bmw'
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Dropdown($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['dropdown']['required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $option_name = $key.'drop';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Dropdown_Whitelist_Validator($option_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }
    }
    
    public function test_dropdown_custom_default(){
        $args = array(
            'id' => 'post_usage_dropdown',
            'type' => 'dropdown',
            'title' => 'Dropdown Field',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'option_name' => 'post_usage_second_dropdown',
            'dropdown_options' => array(
                "volvo" => "Volvo",
                "saab" => "Saab",
                "bmw" => "BMW"
            ),
            'is_required' => TRUE,
            'default_value' => 'custom_value'
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Dropdown($args);      
               
        $scenarios = include('data/whitelist_field_scenarios.php');
        $scenarios = $scenarios['dropdown']['custom_default_value']['required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $option_name = $key.'drop_custom_default';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Dropdown_Whitelist_Validator($option_name, array('sanitize_callback_default' => 'custom_value'), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($option_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }
    }


}