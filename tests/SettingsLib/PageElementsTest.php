<?php

class PageElementsTest extends WP_UnitTestCase{
    
    private $fields;
    private $page_elements_holder;
    private $page_elements;
    
    public function setUp(){
        parent::setUp();
        $page_elements = include('data/page_elements.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->page_elements_holder->configure();
        $this->fields = $this->page_elements_holder->get_fields();
        $this->page_elements = $this->page_elements_holder->get_elements();
    }
    
    public function test_field_creation(){
        $this->assertCount(9, $this->fields);
    }
    
    public function test_get_elements(){
        $this->assertCount(11, $this->page_elements);
    }
    
    public function test_get_existing_option_value(){
        $field = current($this->fields);
        $title = $field->get_option_value('title');
        $this->assertEquals('Checkbox 2*', $title);
    }
    
    public function test_get_non_existing_option_value(){
        $field = current($this->fields);
        $this->assertEquals(FALSE, $field->get_option_value('anything'));
    }
    
}

