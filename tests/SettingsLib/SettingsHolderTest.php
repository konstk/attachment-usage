<?php

class SettingsHolderTest extends WP_UnitTestCase{
    
    private $settings_holder;
    private $settings;
    
    public function setUp(){
        parent::setUp();
        $page_elements = include('data/page_elements.php');
        $settings = include('data/settings.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->fields = $this->page_elements_holder->get_fields();
        $this->settings_holder = new AttachmentUsage\SettingsLib\Settings_Holder($settings, $this->fields);
        $this->settings = $this->settings_holder->get_settings();
    }
    
    public function test_get_settings(){
        $this->assertCount(9, $this->settings);
    }
    
    public function test_get_option_settings_not_registered(){
        $this->assertEquals(FALSE, get_option('post_usage_cb_setting'));
    }
    
    public function test_get_option_settings_registered(){
        $this->assertEquals('', get_option('post_usage_cb_setting'));
    }
    
}

