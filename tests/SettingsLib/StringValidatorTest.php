<?php

class StringValidatorTest extends WP_UnitTestCase{
    
    private $setting;
    private $option_group;
    private $option_name;
    private $field;
    private $fields;
    private $is_required = TRUE;
    
    public function setUp(){
        parent::setUp();
        $this->option_group = 'post_usage_page';
        $this->option_name = 'option';
        $page_elements = include('data/page_elements.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->page_elements_holder->configure();
        $this->fields = $this->page_elements_holder->get_fields();
        $this->field = current($this->fields);                      
    }
        
    
    public function test_validate_input_required(){
        $args = array(
            'id' => 'post_usage_input',
            'type' => 'input',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_usage_cb_setting',
            'is_required' => $this->is_required
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Input($args);      
               
        $scenarios = include('data/string_field_scenarios.php');
        $scenarios = $scenarios['input']['required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $group_name = $key.'input_required';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\String_Validator($group_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($group_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }       
    }
    
    public function test_validate_input_not_required(){
        $args = array(
            'id' => 'post_usage_input',
            'type' => 'input',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_usage_cb_setting',
            'is_required' => FALSE
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Input($args);      
               
        $scenarios = include('data/string_field_scenarios.php');
        $scenarios = $scenarios['input']['not-required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $group_name = $key.'input_not_required';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\String_Validator($group_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($group_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }       
    }
    
    public function test_validate_textarea_required(){
        $args = array(
            'id' => 'post_usage_itext_3nput',
            'type' => 'textarea',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_textarea_setting',
            'is_required' => $this->is_required
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Textarea($args);      
               
        $scenarios = include('data/string_field_scenarios.php');
        $scenarios = $scenarios['textarea']['required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $group_name = $key.'textarea_required';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\String_Validator($group_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($group_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }       
    }
    
    public function test_validate_textarea_not_required(){
        $args = array(
            'id' => 'post_usage_text_2_input',
            'type' => 'textarea',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_not_required_textare_setting',
            'is_required' => FALSE
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\Textarea($args);      
               
        $scenarios = include('data/string_field_scenarios.php');
        $scenarios = $scenarios['textarea']['not-required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $group_name = $key.'text_not_required';
            $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\String_Validator($group_name, array(), $field);
            $validation = $whitelist_validator->validate($val['value']);
            $errors = get_settings_errors($group_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);          
        }       
    }

}