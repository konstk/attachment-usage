<?php

class FileValidatorTest extends WP_UnitTestCase{
    
    private $setting;
    private $option_group;
    private $option_name;
    private $field;
    private $fields;
    private $is_required = TRUE;
    
    public function setUp(){
        parent::setUp();
        $this->option_group = 'post_usage_page';
        $this->option_name = 'option';
        $page_elements = include('data/page_elements.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->page_elements_holder->configure();
        $this->fields = $this->page_elements_holder->get_fields();
        $this->field = current($this->fields);                      
    }        
    
    public function test_validate_file_required(){
        $args = array(
            'id' => 'post_usage_itext_3nput',
            'type' => 'file',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_file_setting',
            'is_required' => $this->is_required,
            'default_value' => 0
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\File($args);      
               
        $scenarios = include('data/file_field_scenarios.php');
        $scenarios = $scenarios['file']['required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $group_name = $key.'file_required';
            $file_validator = new AttachmentUsage\SettingsLib\Elements\Validators\File_Validator($group_name, array(), $field);
            $validation = $file_validator->validate($val['value']);
            $errors = get_settings_errors($group_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);
        }       
    }
    
    public function test_validate_file_not_required(){
        $args = array(
            'id' => 'post_usage_text_2_input',
            'type' => 'file',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_not_required_file_setting',
            'is_required' => FALSE,
            'default_value' => 0
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\File($args);      
               
        $scenarios = include('data/file_field_scenarios.php');
        $scenarios = $scenarios['file']['not-required'];
        
        if($args['is_required']){
            $args['title'] = $args['title'].'*';
        }
        
        foreach($scenarios as $key => $val){
            #var_dump("Current Line: ".$key);
            $group_name = $key.'file_not_required';
            $file_validator = new AttachmentUsage\SettingsLib\Elements\Validators\File_Validator($group_name, array(), $field);
            $validation = $file_validator->validate($val['value']);
            $errors = get_settings_errors($group_name);
            $this->assertEquals(sprintf($val['error'], $args['title']), end($errors)['message']);
            $this->assertEquals($val['output'], $validation);          
        }       
    }
    
    public function test_valid_file_id(){
        include_once dirname(__FILE__).'/../Attachment.php';
        $attachment = new Attachment('dummy_data/file.pdf');
        $attachment_id = $attachment->get_attachment_id();
        
        $args = array(
            'id' => 'post_usage_text_2_input',
            'type' => 'file',
            'title' => 'Input 2',
            'calback' => '',
            'page' => 'post_usage_page',
            'section' => 'post_usage',
            'template' => 'other_cb',
            'option_name' => 'post_not_required_file_setting_nen',
            'is_required' => FALSE
        );
        $field = new AttachmentUsage\SettingsLib\Elements\Fields\File($args);
        $group_name = 'valid_agb_file';
        $whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\File_Validator($group_name, array(), $field);
        $validation = $whitelist_validator->validate($attachment_id);
        $errors = get_settings_errors($group_name);
        $this->assertEquals(NULL, end($errors)['message']);
        $this->assertEquals($attachment_id, $validation);      
    }
    
    

}