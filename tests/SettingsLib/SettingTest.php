<?php

class SettingTest extends WP_UnitTestCase{
    
    private $setting;
    private $option_group;
    private $option_name;
    private $field;
    private $fields;
    
    public function setUp(){
        parent::setUp();
        $this->option_group = 'post_usage_page';
        $this->option_name = 'option';
        $page_elements = include('data/page_elements.php');
        $this->page_elements_holder = new AttachmentUsage\SettingsLib\Page_Elements_Holder($page_elements);
        $this->page_elements_holder->configure();
        $this->fields = $this->page_elements_holder->get_fields();
        $this->field = current($this->fields);
    }
    
    public function test_setting_creation_without_args(){
        $setting = new AttachmentUsage\SettingsLib\Setting(
                $this->option_group,
                $this->option_name,
                $this->field);
        $this->assertEquals(NULL, $setting->get_args());
        $this->assertEquals($this->option_name, $setting->get_option_name());
        $this->assertEquals($this->option_group, $setting->get_option_group());
    }
    
    public function test_setting_creation_with_args(){
        $args = array(
            'sanitize_callback_default' => 'second_cb',
            'sanitize_callback_type' => 'whitelist',
            'sanitize_callback_message' => __('Do not change value of checkboxes')
            );
        $setting = new AttachmentUsage\SettingsLib\Setting(
                $this->option_group,
                $this->option_name,
                $this->field,
                $args);
        $this->assertNotEquals(NULL, $setting->get_args());
        $this->assertEquals($this->option_name, $setting->get_option_name());
        $this->assertEquals($this->option_group, $setting->get_option_group());

    }
}
