<?php

$args = array(
        'id' => 'post_usage_cb2',
        'type' => 'checkbox',
        'title' => 'Checkbox 2',
        'calback' => '',
        'page' => 'post_usage_page',
        'section' => 'post_usage',
        'template' => 'other_cb',
        'group' => array(
            0 => array('id' => 'cb1', 'value' => 'first_cb'),
            1 => array('id' => 'cb2', 'value' => 'second_cb'),
            2 => array('id' => 'cb3', 'value' => 'third_cb')
            ),
        'option_name' => 'post_usage_cb_setting',
        'is_required' => TRUE,
        'group_option_name' => 'post_usage_cb_setting[]'
    );
$field = new AttachmentUsage\SettingsLib\Elements\Fields\Checkbox($args);      
$whitelist_validator = new AttachmentUsage\SettingsLib\Elements\Validators\Whitelist_Validator('test', array(), $field);

$scenarios = include('data/field_scenarios.php');
foreach($scenarios as $key => $val){
    if(array_key_exists('error', $val)){
        $validation = $whitelist_validator->validate($val['value']);
        $errors = get_settings_errors('test');
        sprintf("Expected %s1 and actual %s2", $val['error'], $errors[0]['message']);
        #$this->assertEquals($val['error'], $errors[0]['message']);
        #$this->assertEquals($val['output'], $validation);
    }else{
        $validation = $whitelist_validator->validate($val['value']);
        #$this->assertEquals($val['output'], $validation);
    }
}

