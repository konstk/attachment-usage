<?php

class AttachmentTypeTest extends WP_UnitTestCase{
    
    private $files = array(
        'image' => array(
            'path' => 'dummy_data/bild1-compressed.jpg', 'type' => 'image'
            ),
        'audio' => array(
            'path' => 'dummy_data/audio.mp3', 'type' => 'audio'
            ),
        'video' => array(
           'path' => 'dummy_data/video.mp4', 'type' => 'video'
            ),
        'archive' => array(
            'path' => 'dummy_data/archive.zip', 'type' => 'archive'
            ),
        'table' => array(
            'path' => 'dummy_data/excel.xlsx', 'type' => 'table'
            ),
        'document' => array(
            'path' => 'dummy_data/file.pdf', 'type' => 'document'
            )
    );
    
    
    public function setUp(){
       parent::setUp(); 
    }
    
    public function test_get_attachment_type_from_media_file(){
        foreach($this->files as $file){
            include_once dirname(__FILE__).'/Attachment.php';
            $attachment = new Attachment($file['path']);
            $attachment_id = $attachment->get_attachment_id();
            $post = get_post($attachment_id);
            $file_type = $post->post_mime_type;
            $attachment_type_obj = new AttachmentUsage\Core\Attachment_Type($file_type);
            $this->assertEquals($file['type'], $attachment_type_obj->get_attachment_type());
        }
    }
    
}
