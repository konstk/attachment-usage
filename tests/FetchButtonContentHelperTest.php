<?php

class FetchButtonContentHelperTest extends WP_UnitTestCase{
    
    private $fetch_btn_content_obj;    
    
    public function setUp(){
       parent::setUp();
       $this->fetch_btn_content_obj = new AttachmentUsage\Core\Fetch_Button_Content_Helper(5);
    }
    
    
    public function test_get_button(){
        $this->assertNotEmpty($this->fetch_btn_content_obj->get_button());
    }
    
    public function test_get_button_with_result(){
        $string = $this->fetch_btn_content_obj->get_button().'<span class="spinner"></span><br>'
                . '<div class="attachment-usage-wrapper">Attachment not found</div>';
        $this->assertEquals($this->fetch_btn_content_obj->get_button_with_result('Attachment not found'), $string);
    }
    
}